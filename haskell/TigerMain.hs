module Main (main) where
import qualified System.Environment as Env
import System.Console.GetOpt
import Control.Monad

import TigerParser
import TigerEscap
import TigerAbs
import TigerPretty

import Text.Parsec (runParser)

data Flag = Arbol

data Options = Options {
        optArbol :: Bool
        ,optDebEscap :: Bool
    }
    deriving Show

defaultOptions :: Options
defaultOptions = Options {optArbol = False, optDebEscap = False }

options :: [OptDescr (Options -> Options)]
options = [ Option ['a'] ["arbol"] (NoArg (\opts -> opts {optArbol = True})) "Muestra el AST luego de haber realizado el cálculo de escapes"
            , Option ['e'] ["escapada"] (NoArg (\opts -> opts {optDebEscap = True})) " Stepper escapadas..."]

compilerOptions :: [String] -> IO (Options, [String])
compilerOptions argv = case getOpt Permute options argv of
                        (o,n,[]) -> return (foldl (flip id) defaultOptions o, n)
                        (_,_,errs) -> ioError (userError (concat errs ++ usageInfo header options))
    where
        header = "Se usa: tiger fileName [OPTIONS] "

showExp :: Exp -> IO ()
showExp e = do
    putStrLn "AST!***** Martín Ponete las pilas hace un PP"
    putStrLn $ renderExp e
    putStrLn "FIN ----"

calculoEscapadas rawAST opt = do
                if (optDebEscap opt) then
                    case (debbugEnv rawAST) of
                    (Left errEsc) -> do
                        putStrLn "Error en el calculo de variables escapadas:"
                        putStrLn $ show errEsc 
                    (Right (exp,envs)) -> do
                        putStrLn "Stepper MODE!!! Bienvenidos a la fiesta de las variables escapadas"
                        mapM_ ((\str -> putStrLn str >> putStrLn "-------") . show) (reverse (e envs))
                        putStrLn "yes!!!"
                else
                    case (calcularEEsc rawAST) of
                        (Left errEsc) -> do
                            putStrLn "Error en el calculo de variables escapadas:"
                            putStrLn $ show errEsc 
                        (Right escap) -> do
                            when (optArbol opt) (showExp escap)
                            (putStrLn "yes!!!")

main = do
    s:opts <- Env.getArgs
    (opts', err) <- compilerOptions opts
    putStrLn ("Opciones:" ++ show opts')
    sourceCode <- readFile s
    let rawEAST = runParser expression () s sourceCode
    case rawEAST of
        (Left err) -> do
                    putStrLn "Error en el parser:"
                    putStrLn $ show err
        (Right rawAST) -> calculoEscapadas rawAST opts'
